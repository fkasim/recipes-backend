// SCRAM mongoDB cloud authentication : fkasim/25bmlkoq08Zkz4mD
// https://cloud.mongodb.com/

const express = require('express');
const bodyParser = require('body-parser');
const Recipe = require('./models/recipe');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');

// Connect our API to MongoDB cluster
mongoose.connect('mongodb+srv://fkasim:25bmlkoq08Zkz4mD@fkcluster-hsrxh.mongodb.net/test?retryWrites=true', { useNewUrlParser: true })
  .then(() => {
    console.log('Successfully connected to MongoDB Atlas!');
  })
  .catch((error) => {
    console.log('Unable to connect to MongoDB Atlas!');
    console.error(error);
});

app.use(bodyParser.json());
app.use(cors());

// Save recipe into database
app.post('/api/recipes', (req, res, next) => {
    const recipe = new Recipe({
      title: req.body.title,
      time: req.body.time,
      difficulty: req.body.difficulty,
      ingredients: req.body.ingredients,
      instructions: req.body.instructions
    });
    recipe.save().then(
      () => {
        res.status(201).json({
          message: 'Recipe saved successfully!'
        });
      }
    ).catch(
      (error) => {
        res.status(400).json({
          error: error
        });
      }
    );
});

// Modify recipe into database
app.put('/api/recipes/:id', (req, res, next) => {
    const recipe = new Recipe({
      _id: req.params.id,
      title: req.body.title,
      time: req.body.time,
      difficulty: req.body.difficulty,
      ingredients: req.body.ingredients,
      instructions: req.body.instructions
    });
    Recipe.updateOne({_id: req.params.id}, recipe).then(
      () => {
        res.status(201).json({
          message: 'Recipe updated successfully!'
        });
      }
    ).catch(
      (error) => {
        res.status(400).json({
          error: error
        });
      }
    );
});

// Returns the recipe with the provided ID
app.get('/api/recipes/:id', (req, res, next) => {
    Recipe.findOne({
      _id: req.params.id
    }).then(
      (recipe) => {
        res.status(200).json(recipe);
      }
    ).catch(
      (error) => {
        res.status(404).json({
          error: error
        });
      }
    );
});

// Returns the recipe with the provided ID
app.delete('/api/recipes/:id', (req, res, next) => {
    Recipe.deleteOne({
      _id: req.params.id
    }).then(
      (recipe) => {
        res.status(200).json({
            message: 'Deleted'
        });
      }
    ).catch(
      (error) => {
        res.status(404).json({
          error: error
        });
      }
    );
});

// Returns all recipes
app.get('/api/recipes', (req, res, next) => {
    Recipe.find().then(
      (recipes) => {
        res.status(200).json(recipes);
      }
    ).catch(
      (error) => {
        res.status(400).json({
          error: error
        });
      }
    );
});

module.exports = app;